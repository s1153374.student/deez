using System;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {
        [Fact]
        public void BasePremiumIsCorrect()
        {
            // Arrange
            Vehicle vehicle = new(200, 3000, 3000);
            double expectedResult = 23;

            // Act
            double result = CalculateBasePremium(vehicle);

            // Assert
            Assert.Equal(expectedResult, result);
        }

        // A Raise of 15% should be applied if the owner is < 23 years old OR if the license is < 5 years old
        [Theory]
        [InlineData(10, 25, 23)]    // No raise so 23
        [InlineData(1, 25, 26.45)]  // License raise so 26.45   [23 + 3.45 (15% of 23)]
        [InlineData(10, 22, 26.45)] // Age raise so 26.45       [23 + 3.45 (15% of 23)]
        public void IsAgeOrLicenseRaiseApplied(int licenseAge, int policyAge, double expectedResult)
        {
            // Arrange
            var policyMoq = new Mock<IPolicyHolder>();
            policyMoq.Setup(policy => policy.Age).Returns(policyAge);
            policyMoq.Setup(policy => policy.LicenseAge).Returns(licenseAge);
            policyMoq.Setup(policy => policy.PostalCode).Returns(4500);
            policyMoq.Setup(policy => policy.NoClaimYears).Returns(0);

            Vehicle vehicle = new(200, 3000, 3000);

            // Act
            PremiumCalculation result = new(vehicle, policyMoq.Object, InsuranceCoverage.WA);

            // Assert
            Assert.Equal(expectedResult, result.PremiumAmountPerYear);
        }

        // Extra premium should be applied based on the postal code
        [Theory]
        // 5% (10xx - 35xx OR 1000 - 3599) [23 + 1.15]
        [InlineData(24.15, 1000)]
        [InlineData(24.15, 3599)]

        // 2% (36xx - 44xx OR 3600 - 4499) [23 + 0.46]
        [InlineData(23.46, 3600)]
        [InlineData(23.46, 4499)]

        // 0% (< 1000 and > 4499) [23]
        [InlineData(23.00, 0999)]
        [InlineData(23.00, 4500)]

        public void PostalPremiumIsApplied(double expectedResult, int zip)
        {
            // Arrange
            PolicyHolder policy = new(25, "01-01-2000", zip, 0);
            Vehicle vehicle = new(200, 3000, 3000);

            // Act
            PremiumCalculation result = new(vehicle, policy, InsuranceCoverage.WA);

            // Assert
            Assert.Equal(expectedResult, result.PremiumAmountPerYear);
        }

        // WA Plus should be 20% more expensive
        [Fact]
        public void IsWAPlusRaiseApplied()
        {
            // Arrange
            PolicyHolder policy = new(25, "01-01-2000", 4500, 0);
            Vehicle vehicle = new(200, 3000, 3000);

            // Act
            PremiumCalculation result = new(vehicle, policy, InsuranceCoverage.WA_PLUS);

            // Assert
            Assert.Equal(27.6, result.PremiumAmountPerYear); // 120% of 23 is 27.6
        }

        // All Risk has double the premium of WA
        [Fact]
        public void IsAllRiskRaiseApplied()
        {
            // Arrange
            PolicyHolder policy = new(25, "01-01-2000", 4500, 0);
            Vehicle vehicle = new(200, 3000, 3000);

            // Act
            PremiumCalculation result = new(vehicle, policy, InsuranceCoverage.ALL_RISK);

            // Assert
            Assert.Equal(46, result.PremiumAmountPerYear); // 200% of 23 is 46
        }

        // Discount of 5% should be applied per damage free year above 5 (until max of 65%)
        [Theory]
        [InlineData(0, 23)]     // 0%
        [InlineData(5, 23)]     // 0%
        [InlineData(6, 21.85)]  // 5%
        [InlineData(10, 17.25)] // 25%
        [InlineData(100, 8.05)] // 65%
        public void IsDamageDiscountApplied(int years, double expectedResult)
        {
            // Arrange
            PolicyHolder policy = new(25, "01-01-2000", 4500, years);
            Vehicle vehicle = new(200, 3000, 3000);

            // Act
            PremiumCalculation result = new(vehicle, policy, InsuranceCoverage.WA);

            // Assert
            Assert.Equal(expectedResult, result.PremiumAmountPerYear);
        }

        // Is discount of 2.5% applied when paying yearly
        [Fact]
        public void IsYearDiscountApplied()
        {
            // Arrange
            PolicyHolder policy = new(25, "01-01-2000", 4500, 0);
            Vehicle vehicle = new(200, 3000, 3000);
            PremiumCalculation calculation = new(vehicle, policy, InsuranceCoverage.WA);

            // Act
            double year = calculation.PremiumPaymentAmount(PaymentPeriod.YEAR);
            double month = calculation.PremiumPaymentAmount(PaymentPeriod.MONTH);

            // Assert
            Assert.Equal(month, ((year * 1.025) / 12), 2);
        }

        // Premium should be rounded to no more than two decimal points
        [Fact]
        public void IsPremiumRounded()
        {
            // Arrange
            PolicyHolder policy = new(25, "01-01-2000", 4500, 0);
            Vehicle vehicle = new(200, 3000, 3000);
            double expectedResult = 27.60;

            // Act
            PremiumCalculation result = new(vehicle, policy, InsuranceCoverage.WA_PLUS);

            // Assert
            Assert.Equal(expectedResult, result.PremiumAmountPerYear);
        }
    }
}
