using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class VehicleTest
    {
        [Theory]
        [InlineData(-1000, 1000)]
        [InlineData(0, 0)]
        [InlineData(1000, 0)]
        public void IsAgeValid(int year, int expectedResult)
        {
            // Arrange
            int testYear = DateTime.Now.Year + year;
            Vehicle vehicle = new(235, 3000, testYear);

            // Act + Assert
            Assert.Equal(expectedResult, vehicle.Age);
        }
    }
}
