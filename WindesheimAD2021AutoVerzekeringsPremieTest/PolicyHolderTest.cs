using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData(23)]
        [InlineData(20)]
        [InlineData(17)]
        public void AgeByDateIsValid(int expectedResult)
        {
            // Arrange
            string dateStr = DateTime.Now.AddYears(-expectedResult).ToString("dd-MM-yy");
            PolicyHolder holder = new(24, dateStr, 1328, 5);

            // Act + Assert
            Assert.Equal(holder.LicenseAge, expectedResult);
        }
    }
}
