﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    class PolicyHolder : IPolicyHolder
    {
        public int Age { get; private set; }
        public int LicenseAge { get; private set; }
        public int PostalCode { get; private set; }
        public int NoClaimYears { get; private set; }

        internal PolicyHolder(int Age, string DriverlicenseStartDate, int PostalCode, int NoClaimYears )
        {
            this.Age = Age;
            this.PostalCode = PostalCode;
            this.NoClaimYears = NoClaimYears;
            LicenseAge = AgeByDate(ParseDate(DriverlicenseStartDate));
        }

        private static DateTime ParseDate(string dateStr)
        {
            var cultureInfo = new CultureInfo("nl-NL");
            DateTime value;

            if (DateTime.TryParse(dateStr, cultureInfo, DateTimeStyles.None, out value))
            {
                return value;
            } 
            else
            {
                throw new ArgumentException(nameof(dateStr));
            }
        }

        private static int AgeByDate(DateTime date)
        {
            // Original fix i tried that i liked more but it caused a compile error in stryker so i removed it :(
            // TimeSpan span = DateTime.Now - date;
            // double years = span.TotalDays / 365.25;
            // return Convert.ToInt32(Math.Floor(years));

            return Math.Max(0, DateTime.Today.Year - date.Year);
        }

    }
}
